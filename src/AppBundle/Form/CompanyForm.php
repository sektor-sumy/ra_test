<?php
namespace AppBundle\Form;

use AppBundle\Entity\Country;
use AppBundle\Entity\Process;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints;

/**
 * Class CompanyForm
 */
class CompanyForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Name',
                'required' => true,
                'constraints' => [
                    new Constraints\NotBlank(),
                ],
                'attr' => [
                    'placeholder' => 'Name',
                    'class' => 'form-control'
                ],
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Description',
                'required' => false,
                'constraints' => [
                    new Constraints\NotBlank(),
                ],
                'attr' => [
                    'placeholder' => 'Description',
                    'class' => 'form-control'
                ],
            ])
            ->add('process', EntityType::class, [
                'label' => 'Process',
                'choice_label' => 'name',
                'class' => Process::class,
                'constraints' => [
                    new Constraints\NotBlank(),
                ],
                'attr' => [
                    'placeholder' => 'Process',
                    'class' => 'form-control'
                ],
            ]);
    }
}
