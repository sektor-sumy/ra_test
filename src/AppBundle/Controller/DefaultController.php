<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @Template()
     * @return array
     */
    public function indexAction()
    {
        $companies = $this->getDoctrine()->getRepository('AppBundle:Company')->findBy([], ['id' => 'ASC']);

        return [
            'companies' => $companies,
        ];
    }
}
