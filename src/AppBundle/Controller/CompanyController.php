<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Company;
use AppBundle\Form\CompanyForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/company")
 */
class CompanyController extends Controller
{

    /**
     * @Route("/add", name="company-add")
     * @Template()
     * @param Request $request
     * @return array
     * @throws \Exception
     */
    public function addAction(Request $request)
    {
        $form = $this->createForm(CompanyForm::class, new Company());
        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $this->getDoctrine()->getManager()->beginTransaction();
                try {
                    /** @var Company $company */
                    $company = $form->getData();
                    $this->getDoctrine()->getManager()->persist($company);
                    $this->getDoctrine()->getManager()->flush();
                    $this->getDoctrine()->getManager()->commit();

                    return $this->redirectToRoute('homepage');
                } catch (\Exception $e) {
                    $this->getDoctrine()->getManager()->rollback();
                    throw $e;
                }

            }
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("/edit/{id}", name="company-edit", requirements={"id": "^\d+$" })
     * @Template()
     * @param Company $company
     * @param Request $request
     * @return array
     * @throws \Exception
     */
    public function editAction(Company $company, Request $request)
    {
        $form = $this->createForm(CompanyForm::class, $company);
        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $this->getDoctrine()->getManager()->beginTransaction();
                try {
                    /** @var Company $company */
                    $company = $form->getData();
                    $this->getDoctrine()->getManager()->flush();
                    $this->getDoctrine()->getManager()->commit();

                    return $this->redirectToRoute('homepage');
                } catch (\Exception $e) {
                    $this->getDoctrine()->getManager()->rollback();
                    throw $e;
                }

            }
        }

        return [
            'form' => $form->createView(),
        ];
    }
}
