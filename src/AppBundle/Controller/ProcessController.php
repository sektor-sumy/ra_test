<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Process;
use AppBundle\Form\ProcessForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/process")
 */
class ProcessController extends Controller
{

    /**
     * @Route("/add", name="process-add")
     * @Template()
     * @param Request $request
     * @return array
     * @throws \Exception
     */
    public function addAction(Request $request)
    {
        $form = $this->createForm(ProcessForm::class, new Process());
        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $this->getDoctrine()->getManager()->beginTransaction();
                try {
                    /** @var Process $process */
                    $process = $form->getData();
                    $this->getDoctrine()->getManager()->persist($process);
                    $this->getDoctrine()->getManager()->flush();
                    $this->getDoctrine()->getManager()->commit();

                    return $this->redirectToRoute('homepage');
                } catch (\Exception $e) {
                    $this->getDoctrine()->getManager()->rollback();
                    throw $e;
                }
                
            }
        }

        return [
            'form' => $form->createView(),
        ];
    }
}
